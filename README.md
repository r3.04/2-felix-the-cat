# TP n°2 : Pratique git

*Note : Ce TP pourra participer à votre note d'UE6 dans cette ressource.*

Dans ce TP, vous allez mettre en pratique l’outil de gestion de version git, et notamment les branches.

L’exercice consiste à faire un programme à plusieurs en parallèle en utilisant les branches et le merge de git.
Nous vous imposons des contraintes qui sont existantes dans certains projets du « monde réel » (ex : projets open-source) et qui ont pour but de vous donner une expérience des commandes git.

## Règles du jeu

 - Le projet est à faire par groupe de 4 (éventuellement 5, exceptionnellement 3) ;
 - Vous ne pouvez pas communiquer oralement (situation courante d'un projet en groupe à distance);
 - Deux membres d’un groupe ne peuvent pas être voisins (pour éviter d'être tenté de déroger au point précédent) ;
 - Le projet est décomposé en plusieurs petites parties qui seront implémentées dans des méthodes différentes ;
 - Chaque membre du groupe travaillera sur sa propre branche puis fusionnera son travail (*merge*) sur la branche commune (master) ;
 - Vous ferez des commits et des push le plus souvent possible ;
 - Il y aura des conflits, c’est normal et cela fait partie de l’exercice.

## Mise en place

### Formation du groupe

Inscrivez au tableau votre nom sur un groupe en veillant bien à respecter les contraintes de taille et de voisinage.
|n°|    A    |    B    |    C    |    D    |   (E)   |
|--|---------|---------|---------|---------|---------|
|Équipe 1 | Gael    | Leo     | Axelle  | Ylies   |         |
|Équipe 2 | ...     | ...     | ...     | ...     | ...     |

### Configuration Git

Attention, toutes les opérations concernant votre dépôt local sont à réaliser en ligne de commande (et sans autre outil). Celles qui concernent le dépôt distant sont réalisées via l'interface web de gitlab.

1. Le membre A (et uniquement lui) crée une divergence (*fork*) de visibilité interne du présent projet
2. Le membre A octroie les droits de *owner* du projet aux membres B, C, D voire E et le rôle de *reporter* à l'enseignant de TP
3. **Tous** les membres du projet clônent **sur leur session** le projet **du groupe** (le *fork*), de manière à pouvoir faire des push par la suite sur le bon dépôt distant sans étape de configuration.
4. **Tous** les membres du projet créent **localement** une branche à leur nom et se positionnent dessus
```
git branch <nom-de-votre-branche>
git checkout <nom-de-votre-btranche>
```

## Utilisation de git : fichiers différents

Pas la peine d'ouvrir d'IDE pour l'instant, on va commencer à se familiariser à la manipulation de branches avec de simples fichiers textes.

### Premier push

Depuis sa branche en local donc, chaque membre du groupe va ajouter dans le répertoire *team* un fichier *prenom-nom.txt* (nommé bien sûr avec ses propres prénom et nom) et qui contiendra uniquement le nom de son langage préféré.
Une fois ceci fait, ajoutez votre fichier au suivi (via *git add*) puis faites immédiatement un commit sur votre branche avec un message significatif.

```
git commit -m "premiere implementation de …"
```
Il est désormais temps de partager votre mise à jour (*git push*). Vous devriez avoir une erreur, car votre branche n’existe pas sur le dépôt distant (*remote*). Une indication sur la commande à réaliser pour pouvoir procéder au *push* vous est alors proposée, suivez-la.

### Fusion (Merge)

Tous les membres d’une équipe doivent récupérer les modifications de leurs camarades. Dans la « vraie vie » une seule personne pourrait faire le travail pour tous. Ici, pour apprendre à le faire, tout le monde fera le même travail en parallèle.

Donc commencez par :

```
git pull
```

Naviguez entre les branches
```
git checkout <autre-branche>
```
pour vérifier que le répertoire *team* contient bien des fichiers différents dans chaque branche.

Faites ensuite un merge dans la branche commune (master)
```
git checkout master
git merge <nom-de-la-branche-a-integrer>
```

Il ne devrait pas y avoir de conflit à ce stade (si personne n’a fait de push avant votre pull). Votre branche master devrait être un commit en avance de la branche du dépôt commun. Une fois que vous avez récupéré et fusionné l'ensemble des branches de vos collègues, vous pouvez partager la version unifiée.

```
git push
```

Au besoin, résolvez les conflits (voir plus bas).  Répétez les merges jusqu’à ce que tout le monde ait l'ensemble.

Maintenant que cette étape est supprimée, pensez à supprimez votre propre branche.

## Utilisation de git : même fichier, lignes différentes

Cette fois, on va expérimenter l'écriture dans un même fichier, sur des lignes différentes.
Répétez la procédure vue à l'étape précédente (en repartant de la création d'une branche) mais cette fois sur le présent fichier : *README.MD*
Chaque membre du groupe va compléter ce fichier en ajoutant son prénom et son nom à la ligne qui lui correspond.

 A. 

 B.

 C.

 D.

 E.


## Utilisation de git : même classe, méthodes différentes (Felix the cat)

Dans l'étape qui suit, on va désormais tenter de co-éditer du code source JavaFx.
Attention, quelque soit l'éditeur que vous utiliserez, il est important de renseigner un fichier *.gitignore* de manière à éviter de polluer le projet avec des fichiers de configuration. Dans le cas présent, un *.gitignore* largement relativement exhaustif est déjà fourni. Consultez ce fichier pour voir quels fichiers sont ainsi ignorés de l'historisation.

### Configuration IDE JavaFX

Cette partie du TP s'appuie sur la bibliothèque JavaFX (présente sur les machines de TP dans */home/public/javafx-sdk-17.0.2.* ).
Pour gérer cette dépendance et permettre la déployabilité du projet y compris sur d'autres machines, le choix a été fait de recourir à *maven*. C'est le fichier *pom.xml* du dépôt qui décrit notamment les dépendances et plugins à résoudre.
Ouvrez le projet depuis *IntelliJ IDEA Community Edition*, vous verrez normalement sur la barre verticale à droite de l'écran un *m* (pour *maven*). Ce bouton permet d'ouvrir un volet offrant une vue sur les différents éléments du pom.xml ainsi que leur résolution ou non.

### Sur la même branche : dessinez, c'est fusionné

Pour cette étape, on va changer de méthode et tous les membres vont cette fois travailler sur la même branche.
Créez les méthodes demandées dans Felix.java. Voici un exemple du résultat attendu. La qualité esthétique importe peu, c’est l’exercice git qui est important.

<a>![](img/felix.png)</a>

Chaque étudiant doit implémenter une méthode différente selon la répartition suivante :

| n° | A          | B          | C          | D             | (E)        |
|----|------------|------------|------------|---------------|------------|
| 1  | *drawHead* | *drawEyes* | *drawEars* | *drawWiskers* | *drawNose* |

Un étudiant fait *drawHead*, un autre fait *drawEyes*, … dès que la première version est terminée, partagez-la comme précédemment.

### Conflits

En cas de conflit dans un *merge*, il faut ouvrir le fichier avec un conflit (ici : Felix.java) et intégrer à la main les différentes versions. Git ne peut pas décider tout seul ce qu’il convient de faire.
Vous pouvez travailler avec un éditeur de texte simple, ou avec un outil graphique. Essayez les deux pour voir et comprendre comment faire.
Il faut donc d’abord modifier le fichier « à la main » (décider quoi garder ou comment fusionner les différentes modifications), puis refaire un commit (un message de merge par défaut devrait vous être proposé).

### Replacement

Il peut y avoir des erreurs de placement des éléments. Faites une rotation des méthodes entre les participants (celui qui à fait la tête s’occupe des yeux, celui qui a fait les yeux s’occupe des oreilles,…) pour améliorer les positionnements.

## Branches + Pull/Merge Request

Dans cette partie, on va maintenant intégrer votre code en utilisant un autre mécanisme : la Pull/Merge Request. Pour cela, chaque membre de l'équipe va, dans sa branche personnelle, apporter une modification au code. Par exemple vous pouvez ajouter une souris avec les méthodes pour dessiner le corps, l’oreille, le nez, et l’œil (ou bien ajoutez au chat un chapeau, un nœud papillon, un nez, une bouche, un cigare,…).

<a>![](img/mouse.png)</a>


Faites un commit et un push de vos modifications sur votre branche.

Dans gitlab allez sur le menu des « merge requests » (« Pull request » dans github) pour créer une nouvelle demande.

<a>![](img/merge-request-menu.png)</a>

Indiquez votre branche (à merger dans master) et lancez la comparaison.

<a>![](img/merge-request.png)</a>


Demandez à un autre membre du groupe d’approuver votre requête (normalement après avoir vérifié que le code est correct). (Chacun devrait à tour de rôle approuver et merger une Merge Request).

<a>![](img/merge-approval2.png)</a>

Finalement, la requête peut être mergée. Attention à ne pas détruire votre branche dans ce cas.

<a>![](img/merge-approval.png)</a>


Dans d’autres cas, on peut faire par exemple une branche pour corriger un bug, et si la Pull/Merge Request est approuvée, on pourra détruire immédiatement cette branche.

## Marre de saisir votre mot de passe ? Vive les clés ssh
Si ça n'est pas déjà fait, pensez à utiliser les clés ssh pour vous authentifier sans avoir à saisir identifiants et mot de passe à chaque fois. Pour ce faire, aller sur la [page de paramètrage utilisateur concernant les clés ssh](https://gitlab.univ-lille.fr/-/user_settings/ssh_keys), cliquez sur *Ajoutez une nouvelle clé* et suivez l' [aide proposée](https://gitlab.univ-lille.fr/help/user/ssh#see-if-you-have-an-existing-ssh-key-pair).
