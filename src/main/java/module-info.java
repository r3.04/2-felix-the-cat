module felixthecat {
    requires javafx.controls;
    requires javafx.fxml;


    opens fr.univlille.felixthecat to javafx.fxml;
    exports fr.univlille.felixthecat;
}